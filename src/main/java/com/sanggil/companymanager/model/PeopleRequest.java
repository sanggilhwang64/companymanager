package com.sanggil.companymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class PeopleRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    @Length(min = 11, max = 20)
    private String phone;

    @NotNull
    private LocalDate birthday;
}
