package com.sanggil.companymanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleItem {
    private String name;
    private String phone;
}
