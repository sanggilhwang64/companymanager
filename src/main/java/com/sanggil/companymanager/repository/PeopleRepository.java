package com.sanggil.companymanager.repository;

import com.sanggil.companymanager.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
