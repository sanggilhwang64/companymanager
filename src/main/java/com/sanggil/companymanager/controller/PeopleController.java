package com.sanggil.companymanager.controller;

import com.sanggil.companymanager.model.PeopleItem;
import com.sanggil.companymanager.model.PeopleRequest;
import com.sanggil.companymanager.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService peopleService;

    @PostMapping("/data")
    public String setPeople(@RequestBody @Valid PeopleRequest request) {
        peopleService.setPeople(request.getName(), request.getPhone(), request.getBirthday());
        return "OK";
    }

    @GetMapping("/peoples")
    public List<PeopleItem> getPeoples() {
        List<PeopleItem> result = peopleService.getPeoples();
        return result;
    }
}
